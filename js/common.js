$(document).ready(function() {
    $(function(){
        // Ищем все элементы с class="dial"
        var dials = $(".dial");
        // Перебираем все .dial и пихуем туда canvas с графиком.
        for (i=0; i < dials.length; i++){
            var width = (typeof $(dials[i]).attr("data-width") != 'undefined') ? Math.round($(dials[i]).attr("data-width")) : 80;
            var procent = (Number($(dials[i]).html()) > 0 && Number($(dials[i]).html()) < 100) ? Math.round(Number($(dials[i]).html()) * 10)/10 : 0;
            var lineWidth = (typeof $(dials[i]).attr("data-lineWidth") != 'undefined') ? Number($(dials[i]).attr("data-lineWidth")) : width / 10;
            var size = width+lineWidth;
            var lineRound = (typeof $(dials[i]).attr("data-lineRound") != 'undefined') ? true : false;
            var borderColor = $(dials[i]).css("border-color");
            var color = $(dials[i]).css("color");
            // Меняем размер .dial в зависимости от data-width="80"
            // Устанавливаем размер шрифта так что бы он вмещался в круг не задевая border
            $(dials[i]).css({"width": size + 'px', "height": size + 'px', "font-size": Math.floor((width-lineWidth) / 4) + 'px'});
            // Вставляем canvas такого же размера что и родитель.
            $(dials[i]).html('<canvas id="dial' + i + '" width="' + size + '" height="' + size + '"></canvas><p>' + procent + '%</p>');
            // Выравниваем текст по вертикали
            $("p", dials[i]).css({"line-height": size + 'px'});
            var canvas = document.getElementById("dial" + i);
        var context = canvas.getContext("2d");
            // считаем по формуле радианы
            var radian = 2*Math.PI*procent/100;
            // рисуем круг для фона
            context.arc(width/2+lineWidth/2, width/2+lineWidth/2, width/2, 0, 2*Math.PI, false);
            context.lineWidth = lineWidth;
            context.strokeStyle = borderColor;
            context.stroke();
            context.beginPath();
            // рисуем круг с процентами
            context.arc(width/2+lineWidth/2, width/2+lineWidth/2, width/2, 1.5 * Math.PI, radian+1.5 * Math.PI, false);
            context.strokeStyle = color;
            // Можно скруглить концы отрезка если передан параметр data-lineRound
            if (lineRound == true && lineWidth < width) context.lineCap = "round";
        context.stroke();
        }
    });

    // open toggle block
    $('.toggle-block').each(function() {
        if($(this).hasClass('is-open')) {
            $(this).find('.toggle-block__open__btn').find('span').text('Close detalization')
        } else {
            $(this).find('.toggle-block__open__btn').find('span').text('Show detalization')
        }
    })
    $('.toggle-block__open__btn').on('click', function(){
        const parent = $(this).closest('.toggle-block')
        $(this).toggleClass('active')
        parent.toggleClass('is-open')
        if (parent.hasClass('is-open')) {
            $(this).find('span').text('Close detalization')
        } else {
            $(this).find('span').text('Show detalization')
        }
    })

    // circle chart
    // https://www.cat-in-web.ru/svg-pie-chart/
    /*
 * Создает круговую диаграмму с секторами
 * @param container {DOMElement}
 * @param chartData {Array.<{value: Number, color: String}>}
 * @param config {{strokeWidth: Number, radius: Number, start: String}}
 */
function createPie(container, chartData, config = {}) {
    const svgns = "http://www.w3.org/2000/svg";
    const offsetRatio = {
      top: 0.25,
      right: 0,
      left: 0.5,
      bottom: -0.25,
    }
  
    const strokeWidth = config.strokeWidth || 5;
    const radiusValue = config.radius || 100;
    const radius = radiusValue - strokeWidth / 2;
    const fullSize = 2 * radiusValue;
  
    // длина окружности 
    const length = 2 * Math.PI * radius; 
  
    // смещение начальной точки
    let startPoint = config.start in offsetRatio ? config.start : 'top';
    const chartOffset = length * offsetRatio[startPoint];
  
    // расчетные данные для построения секторов
    const sectors = [];
    chartData.forEach((sectorData, sectorIndex) => {
      // Длина сектора
      const width = length * sectorData.value / 100;
      // Смещение сектора от начальной точки
      let offset = chartOffset;
  
      if (sectorIndex > 0) {
        let prevSector = sectors[sectorIndex - 1];
        offset = prevSector.offset - prevSector.width;
      }
  
      sectors.push({
        ...sectorData,
        width,
        offset,
      })
    });
  
    const svg = createSvgElement('svg', {
      'viewBox': `0 0 ${fullSize} ${fullSize}`,
      'fill': 'none',
      'width': fullSize,
      'height': fullSize,
    });
  
    sectors.forEach(sector => {
      const circle = createSvgElement('circle', {
        cx: radius + strokeWidth / 2,
        cy: radius + strokeWidth / 2,
        r: radius,
        'stroke-dasharray': `${sector.width} ${length - sector.width}`,
        'stroke-dashoffset': sector.offset,
        'stroke': sector.color,
        'stroke-width': strokeWidth
      })
  
      svg.appendChild(circle);
    })
  
    container.appendChild(svg);
  
    function createSvgElement(elementName, attrs = {}) {
      const el = document.createElementNS(svgns, elementName);
      Object.entries(attrs).forEach(([attrName, attrValue]) => {
        el.setAttributeNS(null, attrName, attrValue)
      });
      return el;
    }
  }
  
  
  // разделение на сектора (в процентах)
  const data = [
    {
      value: 20,
      color: '#EB5756'
    },
    {
      value: 15,
      color: '#D2285C'
    },
    {
        value: 20,
        color: '#F1C94B'
      },
    {
      value: 35,
      color: '#6FCF96'
    },
    {
        value: 10,
        color: '#E0E0E0'
      }
  ];
  
  createPie(document.getElementById('chart'), data, {
    strokeWidth: 8,
    radius: 35,
    start: 'top' // top|left|right|bottom
  });
  
})